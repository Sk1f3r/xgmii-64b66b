Emulation of encoding X bytes of random data to XGMII format and then to 64b/66b format
-

##### Script represents a folowing sequence of actions:

  * A number has to be passed to an input to represent a lenght in bytes of Ethernet payload (default 64 bytes or CLI int(argument))
  * Payload then summirized with Ethernet II control information: Preamble, Header, FCS, etc
  * a queue populated with a sequence of random data as wide as sum of a summary of bytes
  * MAC layer takes data, formats to XGMII (sync code, chunk32, chunk32) and puts to a bus
  * PCS layer (PHY) takes data from bus and reformats to 64b/66 format

Queue data formatted in full accordance to a standard of XGMII sync and data codes.

XGMII data then formated in full accordance to "64b/66b vector types table".

##### Example
```text
Bytes in Queue: 0
XGMII bus content: [BitArray('0xff'), BitArray('0x07070707'), BitArray('0x07070707')]
PHY bus content: [BitArray('0b10'), BitArray('0x1e0e1c3870e1c387')]
Bytes in Queue: 90
XGMII bus content: [BitArray('0xc0'), BitArray('0xfb555555'), BitArray('0x55555555')]
PHY bus content: [BitArray('0b10'), BitArray('0x7855555555555555')]
Bytes in Queue: 83
XGMII bus content: [BitArray('0x00'), BitArray('0xd5deadbe'), BitArray('0xef4dadba')]
PHY bus content: [BitArray('0b01'), BitArray('0xd5deadbeef4dadba')]
Bytes in Queue: 75
XGMII bus content: [BitArray('0x00'), BitArray('0xdb00b4fe'), BitArray('0xed0800ab')]
PHY bus content: [BitArray('0b01'), BitArray('0xdb00b4feed0800ab')]
Bytes in Queue: 67
XGMII bus content: [BitArray('0x00'), BitArray('0x39602149'), BitArray('0x0742da7a')]
PHY bus content: [BitArray('0b01'), BitArray('0x396021490742da7a')]
Bytes in Queue: 59
XGMII bus content: [BitArray('0x00'), BitArray('0x9495055d'), BitArray('0x42f7c1eb')]
PHY bus content: [BitArray('0b01'), BitArray('0x9495055d42f7c1eb')]
Bytes in Queue: 51
XGMII bus content: [BitArray('0x00'), BitArray('0xf4150177'), BitArray('0x0a7770a0')]
PHY bus content: [BitArray('0b01'), BitArray('0xf41501770a7770a0')]
Bytes in Queue: 43
XGMII bus content: [BitArray('0x00'), BitArray('0x7a734ac6'), BitArray('0x55c2fef2')]
PHY bus content: [BitArray('0b01'), BitArray('0x7a734ac655c2fef2')]
Bytes in Queue: 35
XGMII bus content: [BitArray('0x00'), BitArray('0xdf592eb7'), BitArray('0x187945b4')]
PHY bus content: [BitArray('0b01'), BitArray('0xdf592eb7187945b4')]
Bytes in Queue: 27
XGMII bus content: [BitArray('0x00'), BitArray('0x3809568a'), BitArray('0xe713b400')]
PHY bus content: [BitArray('0b01'), BitArray('0x3809568ae713b400')]
Bytes in Queue: 19
XGMII bus content: [BitArray('0x00'), BitArray('0xab316e2c'), BitArray('0x866156d1')]
PHY bus content: [BitArray('0b01'), BitArray('0xab316e2c866156d1')]
Bytes in Queue: 11
XGMII bus content: [BitArray('0x00'), BitArray('0xa1aa0624'), BitArray('0xffd90511')]
PHY bus content: [BitArray('0b01'), BitArray('0xa1aa0624ffd90511')]
Bytes in Queue: 3
XGMII bus content: [BitArray('0x1f'), BitArray('0x223344fd'), BitArray('0x07070707')]
PHY bus content: [BitArray('0b10'), BitArray('0xb422334470e1c387')]
Bytes in Queue: 0
XGMII bus content: [BitArray('0xff'), BitArray('0x07070707'), BitArray('0x07070707')]
PHY bus content: [BitArray('0b10'), BitArray('0x1e0e1c3870e1c387')]
```

##### Links

* [[Blog] Articles about Ethernet L1](http://fmad.io/blog-10g-ethernet-layer1-overview.html)
* [[Slides] 8b/10b Coding, 64b/66b Coding](http://slideplayer.com/slide/3309541/)
* [[PDF] Introduction to 10 Gigabit 64b/66b (Clause 49)](https://www.iol.unh.edu/sites/default/files/knowledgebase/10gec/10GbE_Cl49.pdf)
* [[PDF] Scrambled Encoding For 10 GigE](http://www.ieee802.org/3/10G_study/public/june99/bottorff_1_0699.pdf)
* [[PDF] 10 Gigabit Ethernet PCS/PMA v5.0](http://www.xilinx.com/support/documentation/ip_documentation/ten_gig_eth_pcs_pma/v5_0/pg068-ten-gig-eth-pcs-pma.pdf)
* [[PDF] 64b/66b line code](http://www.ieee802.org/3/bn/public/mar13/hajduczenia_3bn_04_0313.pdf)

##### Requirements

* Python 3+ (3.5.2)
* bitstring (3.1.5)

##### Post scriptum

* IFG isn't presented explicitly
* FCS isn't dynamically calculated (yet)
* Bit order isn't modified for the sake of readability
