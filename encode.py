#!/usr/bin/env python3
# Emulation of encoding X bytes of semirandom data to XGMII format and then to 64b/66b format
# Model: Queue <-> MAC <-XGMII-bus-> PCS <-PHY-bus-> PMA <-> MDI
# XGMII encoding is 8b/9b format - 1 bit of ctrl for every 8 bits of data
# 64b/66b encoding - 2 bits of ctrl for every 64 bits of data
#
## IMPORT
from collections import deque
from random import randint
from textwrap import wrap
from sys import argv

from bitstring import BitArray
#
## IP Payload size
if len(argv) > 1:
    PAYLOAD_BYTES = int(argv[1]) # bytes of IP data
else:
    PAYLOAD_BYTES = 64 # Change here
#
## CONST
ETH_L1 = {
    'PREAMBLE': BitArray(hex='55555555555555'),
    'SFD': BitArray(hex='d5')
}
ETH_L2 = {
    'DSTMAC': BitArray(hex='deadbeef4dad'),
    'SRCMAC': BitArray(hex='badb00b4feed'),
    'TYPE': BitArray(hex='0800'),
    'FCS': BitArray(hex='11223344')
}
PAYLOAD = BitArray(bin=''.join([str(randint(0,1)) for _ in range(PAYLOAD_BYTES*8)]))
DATASET = sum([ETH_L1['PREAMBLE'], ETH_L1['SFD'], ETH_L2['DSTMAC'], ETH_L2['SRCMAC'], ETH_L2['TYPE'], PAYLOAD, ETH_L2['FCS']])
#
XGMII_CTRL = {
    'CTRL': BitArray(bin='1'),
    'DATA': BitArray(bin='0')
}
XGMII_SEQ = {
    'IDLE': BitArray(hex='ff'),  # CCCC_CCCC
    'START': BitArray(hex='c0'), # SDDD_DDDD
    'DATA': BitArray(hex='00')   # DDDD_DDDD
}
XGMII_CODE = {
    'I': BitArray(hex='07'), # IDLE
    'S': BitArray(hex='fb'), # START
    'T': BitArray(hex='fd'), # TERMINATE
    'E': BitArray(hex='fe')  # ERROR
}
ENC64_66_CTRL = {
    'DATA': BitArray(bin='01'),
    'CTRL': BitArray(bin='10')
}
ENC64_66_CODE = {
    'C': BitArray(hex='1e0e1c3870e1c387'), # CCCC_CCCC
    'S': BitArray(hex='78'),# SDDD_DDDD
    0: BitArray(hex='87'),  # pos0: TCCC_CCCC
    1: BitArray(hex='99'),  # pos1: DTCC_CCCC
    2: BitArray(hex='aa'),  # pos2: DDTD_CCCC
    3: BitArray(hex='b4'),  # pos3: DDDT_CCCC
    4: BitArray(hex='cc'),  # pos4: DDDD_TCCC
    5: BitArray(hex='d2'),  # pos5: DDDD_DTCC
    6: BitArray(hex='e1'),  # pos6: DDDD_DDTC
    7: BitArray(hex='ff'),  # pos7: DDDD_DDDT
}
FILL = [i for i in wrap(BitArray(hex='1e0e1c3870e1c387').bin, 8)]
## VAR
#
q = deque(maxlen=1500)
xgmii_bus = []
phy_bus = []
#
## CLASS
class QueueClass:
    def __init__(self):
        self.dataset = None
    @property
    def content(self):
        return 'Bytes in Queue: {}'.format(len(q))
    @staticmethod
    def put_data(dataset):
        for i in wrap(dataset.bin, 8):
            q.append(BitArray(bin=i))
#
class MacClass:
    '''
    XGMII is 10 Gigabit Media Independant Interface
    MAC layer sends data formed in 32 bit chunks to PCS
    [XGMII32][clk312][Single data rate][10GBASE-R]
    '''
    def __init__(self):
        self._end_of_stream = False
        self._chunk1, self._chunk2, self._chunks = [], [], []
    @property
    def content(self):
        return 'XGMII bus content: {}'.format(str(xgmii_bus))
    def clear_chunks(self):
        self._chunk1.clear()
        self._chunk2.clear()
        self._chunks.clear()
    def check_queue(self):
        xgmii_bus.clear()
        self.clear_chunks()
        # if new data
        # then add S(0xfb) and x7 D
        if len(q) == len(DATASET.bin)/8:
            xgmii_bus.append(XGMII_SEQ['START']) # 0xc0 for CDDD_DDDD
            self._chunk1.append(XGMII_CODE['S'])
            for _ in range(3):
                self._chunk1.append(q.popleft())
            for _ in range(4):
                self._chunk2.append(q.popleft())
            xgmii_bus.extend([sum(self._chunk1), sum(self._chunk2)])
            return xgmii_bus
        # if there are 8 or more bytes of data
        # then add x8 D
        elif len(q) >= 8:
            # sync code is 0x00 as there are no control information
            xgmii_bus.append(XGMII_SEQ['DATA'])
            # a uniq case when FD at the end of 2nd chunk32
            if len(q) == 8:
                self._end_of_stream = True
            # simply form x2 chunk32
            for _ in range(4):
                self._chunk1.append(q.popleft())
            for _ in range(4):
                self._chunk2.append(q.popleft())
            xgmii_bus.extend([sum(self._chunk1), sum(self._chunk2)])
            return xgmii_bus
        # if less than 8 bytes but not empty
        # then take all, add T and fill with I until chunk32
        elif len(q) < 8 and q:
            # take everything left
            while q:
                self._chunks.append(BitArray(q.popleft()))
            # start forming XGMII sync code with 1 for every 8 bytes of data
            xgmii_bus.append(XGMII_CTRL['DATA']*len(self._chunks))
            # fill sync code with 0 until 8 bits
            while len(xgmii_bus[0]) < 8:
                xgmii_bus[0] += XGMII_CTRL['CTRL']
            # add T after data bytes
            self._chunks.append(XGMII_CODE['T'])
            # and fill all free space with I (0x07)
            while len(self._chunks) < 12:
                 self._chunks.append(XGMII_CODE['I'])
            # split chunk64 to two chunk32
            for i in range(4):
                self._chunk1.append(self._chunks[i])
            for i in range(4,8):
                self._chunk2.append(self._chunks[i])
            xgmii_bus.extend([sum(self._chunk1), sum(self._chunk2)])
            return xgmii_bus
        # if there is no data and not because the end of stream been reached
        # then full of Idle
        elif not q and not self._end_of_stream:
            # add two 32 bits chunks of I
            xgmii_bus.append(XGMII_CTRL['CTRL']*8) # 0xff for CCCC_CCCC
            for _ in range(4):
                self._chunk1.append(XGMII_CODE['I'])
            for _ in range(4):
                self._chunk2.append(XGMII_CODE['I'])
            xgmii_bus.extend([sum(self._chunk1), sum(self._chunk2)])
            return xgmii_bus
        # if there is no data and because the end of stream been reached
        # then Terminate and x7 Idle
        # uniq case when T at the start of a new pair of chunk32
        elif not q and self._end_of_stream:
            self._end_of_stream = False
            # sync code is 0xff as there are only control info
            xgmii_bus.append(XGMII_CTRL['CTRL']*8) # 0xff for TCCC_CCCC
            self._chunk1.append(XGMII_CODE['T'])
            for _ in range(3):
                self._chunk1.append(XGMII_CODE['I'])
            for _ in range(4):
                self._chunk2.append(XGMII_CODE['I'])
            xgmii_bus.extend([sum(self._chunk1), sum(self._chunk2)])
            return xgmii_bus
#
class PcsClass:
    '''
    Physical Coding Sublayer
    A receiver of XGMII encoded data from MAC, encodes to 64b/66b and sends to PMA
    '''
    def __init__(self):
        self.tx_data = None
        # self.to_proceed = []
        self.to_proceed = deque()
        self.align = deque()
    @property
    def content(self):
        return 'PHY bus content: {}'.format(str(phy_bus))
    def check_bus(self):
        self.tx_data = None
        phy_bus.clear()
        self.tx_data = xgmii_bus[1] + xgmii_bus[2]
        # if chunk1 is 07070707
        # then 10 CCCC_CCCC
        if xgmii_bus[1] == XGMII_CODE['I']*4:
            phy_bus.extend([ENC64_66_CTRL['CTRL'], ENC64_66_CODE['C']])
        # if START (0xc0)
        elif xgmii_bus[0] == XGMII_SEQ['START']:
            phy_bus.append(ENC64_66_CTRL['CTRL'])
            phy_bus.append(ENC64_66_CODE['S'] + BitArray(self.tx_data[8:]))
        # if DATA (0x00)
        elif xgmii_bus[0] == XGMII_SEQ['DATA']:
            phy_bus.append(ENC64_66_CTRL['DATA'])
            phy_bus.append(self.tx_data)
        # if pertial control
        # then have to fit bytes wisely and replace 07s
        else:
            # there might be valid 0xfd, so we have to find T from the right side
            # going thru sequence of 0x07
            phy_bus.append(ENC64_66_CTRL['CTRL'])
            # prepare bank with 1 byte chunks to make iterations easier
            for i in wrap(self.tx_data.bin, 8):
                self.align.append(BitArray(bin=i))
            counter = 7
            # cut 07s one by one until 0xFB
            while True:
                tmp = self.align.pop()
                if tmp != XGMII_CODE['I']:
                    break
                counter -= 1
            # now we know the index of T, so its easy to set a correct Block Type field
            # from 64b/66b vector types
            self.to_proceed.append(ENC64_66_CODE[counter])
            # all blocks with data move to a list with awaiting code
            while self.align:
                self.to_proceed.append(self.align.popleft())
            # and populate with parts of '1e0e1c3870e1c387' according to index
            while len(self.to_proceed) < 8:
                self.to_proceed.append(BitArray(bin=FILL[len(self.to_proceed)]))
            phy_bus.append(sum(self.to_proceed))
#
# Testing
def main():
    # init queue, MAC, bus and PCS
    queue, mac, pcs = QueueClass(), MacClass(), PcsClass()
    # queue, mac, xgmii, pcs, phy = QueueClass(), MacClass(), XgmiiBusClass(), PcsClass(), PhyBusClass()
    # ensure that queue been initialized without any data
    print(queue.content)
    # Bytes in Queue: 0
    # then MAC checks a queue
    mac.check_queue()
    # but as it is empty, IDLE is generated
    print(mac.content)
    # XGMII bus content: [BitArray('0xff'), BitArray('0x07070707'), BitArray('0x07070707')]
    # IDLE code and two chunks full of 0x07
    # even if IDLE a PCS encodes it
    pcs.check_bus()
    # check what have been serialized to PCS-PCA bus
    print(pcs.content)
    # PHY bus content: [BitArray('0x10'), [BitArray('0x1e00000000000000')]]
    # Block type 1e and 7 blocks of zeroes are encoded to 64/66b format
    #
    # lets add some data to a queue (change PAYLOAD_SIZE at CONST section)
    queue.put_data(DATASET)
    #
    # process while there are a data in a queue
    for _ in range(int(len(DATASET) / 52)):
        # check current queue length
        print(queue.content)
        # mac checks a queue and process the data [code 0xN, chunk32, chunk32]
        mac.check_queue()
        # XGMII bus content
        print(mac.content)
        # PCS reads the data and encode to 64/66b format
        # pairing a "control XGMII Code" to a type in "64b/66b vector types table"
        pcs.check_bus()
        # PHY bus content
        print(pcs.content)
#
if __name__ == '__main__':
    main()